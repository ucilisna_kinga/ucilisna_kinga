package com.jovsal.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class PostController {

  @Autowired
  PostRepository postRepository;

  @GetMapping("posts/{username}")
  public List<Post> getAllPostForUsername(@PathVariable String username) {
    List<Post> all = postRepository.findAll();
    List<Post> returnedPosts = new ArrayList<>();
    for (Post p : all ) {
      if(p.getCreatedByUsername().equals(username)) {
        returnedPosts.add(p);
      }
    }
    return returnedPosts;
  }

  @PutMapping("post")
  public Post savePost(@RequestBody Post post) {
    post.setId(UUID.randomUUID().toString());
    return postRepository.save(post);
  }

  @GetMapping("insertPostsData")
  public String insertData() {

    postRepository.deleteAll();

    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Јован", "jshalamanoski", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Втор пост од Јован", "jshalamanoski", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Трет пост од Јован", "jshalamanoski", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Учителката ", "teacherFemale", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Втор пост од Учителката ", "teacherFemale", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Ученик ", "studentMale", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Ученичка", "studentFemale", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Родител ", "parentMale", String.valueOf(new Date().getTime()) ));
    postRepository.save(new Post(UUID.randomUUID().toString(), "Прв пост од Родителка ", "parentFemale", String.valueOf(new Date().getTime()) ));

    return "posts data inserted";
  }

  @GetMapping("test")
  public String methodForTesting() {
    return "Post microservice is working";
  }

}
