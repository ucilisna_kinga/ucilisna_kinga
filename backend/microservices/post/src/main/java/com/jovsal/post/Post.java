package com.jovsal.post;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {

  @Id private String id;
  @Column private String text;
  @Column private String createdByUsername;
  @Column private String timeCreated;

  public Post() {}

  public Post(String id, String text, String createdByUsername, String timeCreated) {
    this.id = id;
    this.text = text;
    this.createdByUsername = createdByUsername;
    this.timeCreated = timeCreated;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getCreatedByUsername() {
    return createdByUsername;
  }

  public void setCreatedByUsername(String createdByUsername) {
    this.createdByUsername = createdByUsername;
  }

  public String getTimeCreated() {
    return timeCreated;
  }

  public void setTimeCreated(String timeCreated) {
    this.timeCreated = timeCreated;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Post post = (Post) o;

    if (id != null ? !id.equals(post.id) : post.id != null) return false;
    if (text != null ? !text.equals(post.text) : post.text != null) return false;
    if (createdByUsername != null ? !createdByUsername.equals(post.createdByUsername) : post.createdByUsername != null)
      return false;
    return timeCreated != null ? timeCreated.equals(post.timeCreated) : post.timeCreated == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (text != null ? text.hashCode() : 0);
    result = 31 * result + (createdByUsername != null ? createdByUsername.hashCode() : 0);
    result = 31 * result + (timeCreated != null ? timeCreated.hashCode() : 0);
    return result;
  }
}
