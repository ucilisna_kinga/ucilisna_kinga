package com.jovsal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtFilter implements Filter{

  @Autowired
  JwtFilterUtil jwtFilterUtil;

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    String tokenHeader = jwtFilterUtil.getHeader();

    String authToken = request.getHeader(tokenHeader);
    if(authToken != null) {
      if(jwtFilterUtil.isTokenValidAndFresh(authToken)) {
        filterChain.doFilter(req, res);
      } else {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "UNAUTHORIZED MESSAGE");
      }

    } else {
      String path = jwtFilterUtil.getRelativePath(request.getRequestURL().toString());

      if(jwtFilterUtil.isAllowedPath(path)) {
        filterChain.doFilter(req, res);
      } else {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "UNAUTHORIZED MESSAGE");
      }
    }

  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException { }

  @Override
  public void destroy() { }

}
