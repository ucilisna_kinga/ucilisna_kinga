package com.jovsal.user;

import com.jovsal.security.JwtFilterUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

  @Autowired
  UserRepository userRepository;

  @Autowired
  JwtFilterUtil jwtFilterUtil;

  @GetMapping(value = "/users", produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<User> getUsers() {
    List<User> all = new ArrayList<>();
    all = userRepository.findAll();
    return all;
  }

  @GetMapping(value = "/searchUsers/{searchTerm}", produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<User> getUsers(@PathVariable String searchTerm) {
    List<User> all = userRepository.findAll();
    List<User> returnedUsers = new ArrayList<>();

    if(searchTerm.isEmpty()) {
      return returnedUsers;
    }

    if(!all.isEmpty()) {
      for (User u: all) {
          if(u.getUsername().toLowerCase().startsWith(searchTerm.toLowerCase()) ||
            u.getLastName().toLowerCase().startsWith(searchTerm.toLowerCase()) ||
            u.getFirstName().toLowerCase().startsWith(searchTerm.toLowerCase()) ) {
              returnedUsers.add(u);
          }
      }
    }
    return returnedUsers;
  }

  @GetMapping(value = "/userByUsername/{username}", produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
  public User getUserByUsername(@PathVariable String username, HttpServletRequest request) {
    try{
      User user = userRepository.findOne(username);
      user.setPassword("");
      return user;
    } catch ( NullPointerException e ) {
      return new User();
    }
  }

  @GetMapping(value = "/user", produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
  public User getUser(HttpServletRequest request) {
    String username = jwtFilterUtil.getUsernameFromClaims(request.getHeader(jwtFilterUtil.getHeader()));
    User user = userRepository.findOne(username);
    user.setPassword("");
    return user;
  }




  @GetMapping("/insertUsersData")
  public String insertUsersData() {
    User user1 = new User("jshalamanoski", Integer.toString("passjshalamanoski".hashCode()),
      "Јован", "Шаламаноски", "", "", "male", "teacher", "teacher_male.jpg");

    User user2 = new User("teacherFemale",Integer.toString("passteacherFemale".hashCode()),
      "УчителкаИме", "УчителкаПрезиме", "", "", "female", "teacher", "teacher_female.jpg");

    User user3 = new User("studentMale",Integer.toString("passstudentMale".hashCode()),
      "УченикИме", "УченикПрезиме", "", "", "male", "student", "student_male.jpg");

    User user4 = new User("studentFemale",Integer.toString("passstudentFemale".hashCode()),
      "УченичкаИме", "УченичкаПрезиме", "", "", "female", "student", "student_female.jpg");

    User user5 = new User("parentMale",Integer.toString("passparentMale".hashCode()),
      "РодителИме", "РодителПрезиме", "", "", "male", "parent", "parent_male.jpg");

    User user6 = new User("parentFemale",Integer.toString("passparentFemale".hashCode()),
      "РодителкаИме", "РодителкаПрезиме", "", "", "female", "parent", "parent_female.jpg");


    userRepository.save(user1);
    userRepository.save(user2);
    userRepository.save(user3);
    userRepository.save(user4);
    userRepository.save(user5);
    userRepository.save(user6);

    return "data inserted";
  }

}
