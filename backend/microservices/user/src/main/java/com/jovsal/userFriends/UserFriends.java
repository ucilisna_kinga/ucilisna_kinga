package com.jovsal.userFriends;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_friends")
public class UserFriends {

  @Id String id; // generate uuid with java
  @Column String username;
  @Column String friend;
  @Column String proposal;

  public UserFriends() {
  }

  public UserFriends(String id, String username, String friend, String proposal) {
    this.id = id;
    this.username = username;
    this.friend = friend;
    this.proposal = proposal;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFriend() {
    return friend;
  }

  public void setFriend(String friend) {
    this.friend = friend;
  }

  public String getProposal() {
    return proposal;
  }

  public void setProposal(String proposal) {
    this.proposal = proposal;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    UserFriends that = (UserFriends) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    if (username != null ? !username.equals(that.username) : that.username != null) return false;
    if (friend != null ? !friend.equals(that.friend) : that.friend != null) return false;
    return proposal != null ? proposal.equals(that.proposal) : that.proposal == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (username != null ? username.hashCode() : 0);
    result = 31 * result + (friend != null ? friend.hashCode() : 0);
    result = 31 * result + (proposal != null ? proposal.hashCode() : 0);
    return result;
  }
}
