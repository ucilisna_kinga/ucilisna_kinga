package com.jovsal.security;

import com.jovsal.security.jsonHelpers.ErrorMessage;
import com.jovsal.security.jsonHelpers.JsonResult;
import com.jovsal.security.jsonHelpers.TokenDto;
import com.jovsal.user.User;
import com.jovsal.user.UserCredentialsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {

  @Autowired
  AuthService authService;

  @Autowired
  JwtFilterUtil jwtFilterUtil;

  @PostMapping(value = "/auth", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public JsonResult getNewToken(@RequestBody UserCredentialsDTO userCredentials) {

    if (authService.isUserValid(userCredentials)) {
      String token = authService.generateJwt(jwtFilterUtil.getExpirationHours(), jwtFilterUtil.getSecret(), userCredentials.getUsername());
      TokenDto tokenDto = new TokenDto(token);
      return tokenDto;
    } else {
      return new ErrorMessage("Invalid Credentials");
    }
  }

  @GetMapping(value = "/auth/checkUsername/{username}")
  public ErrorMessage checkUsername(@PathVariable String username) {
    return new ErrorMessage(authService.isUserExist(username).toString());
  }


  @PutMapping("auth/registerUser")
  public User saveUser(@RequestBody User u) {
    return authService.saveUser(u);
  }

}
