package com.jovsal.userFriends;

import com.jovsal.security.JwtFilterUtil;
import com.jovsal.security.jsonHelpers.ErrorMessage;
import com.jovsal.user.User;
import com.jovsal.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class UserFriendsController {

  @Autowired
  JwtFilterUtil jwtFilterUtil;

  @Autowired
  UserFriendsRepository userFriendsRepository;

  @Autowired
  UserRepository userRepository;

  // GET friends // select all where user = username
  @GetMapping(value = "friends", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<User> getAllFriendsByActiveUser(HttpServletRequest request) {
    String username = jwtFilterUtil.getUsernameFromClaims(request.getHeader(jwtFilterUtil.getHeader()));
    List<UserFriends> all = userFriendsRepository.findAll();
    List<User> resultListUsers = new ArrayList<>();
    for (UserFriends uf: all) {
      if(uf.getUsername().equals(username) && uf.getProposal().equals("no")) {
        User one = userRepository.findOne(uf.getFriend());
        one.setPassword(" ");
        resultListUsers.add(one);
      }
    }
    return resultListUsers;
  }

  @GetMapping(value = "friends/{username}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<User> getAllFriendsByUsername(@PathVariable String username, HttpServletRequest request) {
    List<UserFriends> all = userFriendsRepository.findAll();
    List<User> resultListUsers = new ArrayList<>();
    for (UserFriends uf: all) {
      if(uf.getUsername().equals(username) && uf.getProposal().equals("no")) {
        User one = userRepository.findOne(uf.getFriend());
        one.setPassword("");
        resultListUsers.add(one);
      }
    }
    return resultListUsers;
  }

  @GetMapping(value = "friendsRequests/{username}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<User> getAllFriendsRequests(@PathVariable String username) {
    List<UserFriends> all = userFriendsRepository.findAll();
    List<User> resultListUsers = new ArrayList<>();
    for (UserFriends uf: all) {
      if(uf.getUsername().equals(username) && uf.getProposal().equals("yes")) {
        User one = userRepository.findOne(uf.getFriend());
        one.setPassword("");
        resultListUsers.add(one);
      }
    }
    return resultListUsers;
  }

  // PUT friend // insert u1 u2 proposal yes
  @PutMapping(value = "friend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public UserFriends sendProposal(@RequestBody UsernameDto friend, HttpServletRequest request) {
    String username = jwtFilterUtil.getUsernameFromClaims(request.getHeader(jwtFilterUtil.getHeader()));
    UserFriends userFriends = new UserFriends(UUID.randomUUID().toString(), friend.getUsername(), username, "yes");
    UserFriends savedUserFriend = userFriendsRepository.save(userFriends);
    return savedUserFriend;
  }

  // POST accept  // change proposal to no
  @PostMapping(value = "friend", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public UserFriends approveProposal(@RequestBody UsernameDto friend, HttpServletRequest request) {
    String username = jwtFilterUtil.getUsernameFromClaims(request.getHeader(jwtFilterUtil.getHeader()));
    List<UserFriends> all = userFriendsRepository.findAll();
    UserFriends userFriendMatch = new UserFriends("tmpId","tmpUsername","tmpFriend","tmpProposal");
    for (UserFriends uf: all) {
      if(uf.getUsername().equals(username) && uf.getFriend().equals(friend.getUsername())) {
        uf.setProposal("no");
        userFriendMatch = userFriendsRepository.save(uf);
      }
    }

    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(), friend.getUsername() , username, "no" ));

    return userFriendMatch;
  }

  // DELETE friend // delete where user1=u1 user2=u2
  @DeleteMapping(value = "friend/{friendUsername}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public UserFriends deleteFriendship(@PathVariable String friendUsername, HttpServletRequest request) {

    String username = jwtFilterUtil.getUsernameFromClaims(request.getHeader(jwtFilterUtil.getHeader()));
    List<UserFriends> all = userFriendsRepository.findAll();
    UserFriends userFriendMatch = new UserFriends("tmpId","tmpUsername","tmpFriend","tmpProposal");
    for (UserFriends uf: all) {
      if(uf.getUsername().equals(username) && uf.getFriend().equals(friendUsername)) {
        userFriendMatch = uf;
        userFriendsRepository.delete(uf.getId());
      }
      if(uf.getUsername().equals(friendUsername) && uf.getFriend().equals(username)) {
        userFriendMatch = uf;
        userFriendsRepository.delete(uf.getId());
      }
    }
    return userFriendMatch;
  }



  @GetMapping("insertFriendships")
  public String insertFriendships() {
    userFriendsRepository.deleteAll();

    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"jshalamanoski","teacherFemale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"jshalamanoski","studentMale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"jshalamanoski","studentFemale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"jshalamanoski","parentMale","yes"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"jshalamanoski","parentFemale","yes"));

    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"teacherFemale","jshalamanoski","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"teacherFemale","studentMale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"teacherFemale","studentFemale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"teacherFemale","parentMale","yes"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"teacherFemale","parentFemale","yes"));

    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentMale","jshalamanoski","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentMale","teacherFemale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentMale","studentFemale","no"));

    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentFemale","jshalamanoski","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentFemale","teacherFemale","no"));
    userFriendsRepository.save(new UserFriends(UUID.randomUUID().toString(),"studentFemale","studentMale","no"));

    return "Friendships inserted";
  }

}
