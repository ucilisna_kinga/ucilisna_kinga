package com.jovsal.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {

  @Id private String username;
  @Column private String password;
  @Column private String firstName;
  @Column private String lastName;
  @Column private String school;
  @Column private String parentFor;
  @Column private String gender;
  @Column private String profileType;
  @Column private String avatarUrl;


  public User() {
  }

  public User(String username, String password, String firstName, String lastName, String school, String parentFor, String gender, String profileType, String avatarUrl) {
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.school = school;
    this.parentFor = parentFor;
    this.gender = gender;
    this.profileType = profileType;
    this.avatarUrl = avatarUrl;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getSchool() {
    return school;
  }

  public void setSchool(String school) {
    this.school = school;
  }

  public String getParentFor() {
    return parentFor;
  }

  public void setParentFor(String parentFor) {
    this.parentFor = parentFor;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (username != null ? !username.equals(user.username) : user.username != null) return false;
    if (password != null ? !password.equals(user.password) : user.password != null) return false;
    if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
    if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
    if (school != null ? !school.equals(user.school) : user.school != null) return false;
    if (parentFor != null ? !parentFor.equals(user.parentFor) : user.parentFor != null) return false;
    if (gender != null ? !gender.equals(user.gender) : user.gender != null) return false;
    if (profileType != null ? !profileType.equals(user.profileType) : user.profileType != null) return false;
    return avatarUrl != null ? avatarUrl.equals(user.avatarUrl) : user.avatarUrl == null;
  }

  @Override
  public int hashCode() {
    int result = username != null ? username.hashCode() : 0;
    result = 31 * result + (password != null ? password.hashCode() : 0);
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
    result = 31 * result + (school != null ? school.hashCode() : 0);
    result = 31 * result + (parentFor != null ? parentFor.hashCode() : 0);
    result = 31 * result + (gender != null ? gender.hashCode() : 0);
    result = 31 * result + (profileType != null ? profileType.hashCode() : 0);
    result = 31 * result + (avatarUrl != null ? avatarUrl.hashCode() : 0);
    return result;
  }
}
