package com.jovsal.security;

import com.jovsal.user.User;
import com.jovsal.user.UserCredentialsDTO;
import com.jovsal.user.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthService {

  @Autowired
  UserRepository userRepository;

  public Boolean isUserValid(UserCredentialsDTO userCredentials) {

    // get user form db by username
    User dbUser = userRepository.findOne(userCredentials.getUsername());

    // check if the credentials are equals with decoded password
    if (dbUser != null && Integer.toString(userCredentials.getPassword().hashCode()).equals(dbUser.getPassword()) ) {
      return true;
    } else {
      return false;
    }
  }

  public User saveUser(User u){
    u.setPassword(Integer.toString(u.getPassword().hashCode()));
    return userRepository.save(u);
  }


  public String generateJwt(String expirationHours, String secret, String username) {
    Map<String, Object> claims = new HashMap<>();
    claims.put("username", username);
//    claims.put("extraInfo", "extraInfo1");

    Long expirationMillis = Long.valueOf(expirationHours) * 60 * 60 * 1000;

    return Jwts.builder()
      .setClaims(claims)
      .setExpiration(new Date(System.currentTimeMillis() + expirationMillis ))
      .signWith(SignatureAlgorithm.HS512, secret)
      .compact();
  }

  public Boolean isUserExist(String username) {
    User user = userRepository.findOne(username);
    if(user == null) {
      return false;
    } else {
      return true;
    }
  }

}
