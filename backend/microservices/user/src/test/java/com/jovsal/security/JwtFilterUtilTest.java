package com.jovsal.security;

import org.junit.Test;

import static org.junit.Assert.*;

public class JwtFilterUtilTest {
  @Test
  public void getRelativePathTest() throws Exception {
    String urlBase = "http://localhost:8080/" ;
    String urlIco = "http://localhost:8080/favicon.ico" ;
    String urlAuth = "http://localhost:8080/auth";
    String urlAuthSlash = "http://localhost:8080/auth/";
    String urlAuthTest = "http://localhost:8080/auth/test";

    JwtFilterUtil jwtFilterUtil = new JwtFilterUtil();

    assertEquals("/", jwtFilterUtil.getRelativePath(urlBase));
    assertEquals("favicon.ico", jwtFilterUtil.getRelativePath(urlIco));
    assertEquals("auth", jwtFilterUtil.getRelativePath(urlAuth));
    assertEquals("auth", jwtFilterUtil.getRelativePath(urlAuthSlash));
    assertEquals("auth/test", jwtFilterUtil.getRelativePath(urlAuthTest));
  }

  @Test
  public void hashPasswordTest() throws Exception {
    String tmpPass = "passsadsadsa";
    System.out.println(tmpPass.hashCode());
  }

  @Test
  public void checkPasswordTest() throws Exception {
    String pass = "passsadsadsa";
    String hashPassDB = Integer.toString(pass.hashCode());
    System.out.println(hashPassDB);

    // get hashPass form db --- hashPassDB
    System.out.println(Integer.toString(pass.hashCode()).equals(hashPassDB));
    assertEquals(true, Integer.toString(pass.hashCode()).equals(hashPassDB));

  }

  @Test
  public void isTokenValidAndFreshTest() throws Exception {
    String invalidToken = "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1SDE1MTM0MzU5ODksInVzZXJuYW1lIjoidXNlcm5hbWUxIiwiZXh0cmFJbmZvIjoiZXh0cmFJbmZvMSJ9.UlvO90RUflTtiJjBFKkACsL4WX3EdAHRfIwHMBI4A1A6zvdIKzHQnRFpzSfMCXrZnteBk4d0AJOewRQFsK10wA";
    String validExpiredToken = "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDE1MTM0MzU5ODksInVzZXJuYW1lIjoidXNlcm5hbWUxIiwiZXh0cmFJbmZvIjoiZXh0cmFJbmZvMSJ9.UlvO90RUflTtiJjBFKkACsL4WX3EdAHRfIwHMBI4A1A6zvdIKzHQnRFpzSfMCXrZnteBk4d0AJOewRQFsK10wA";
    String validFreshToken = "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDQxMDY1NDI5NTQsInVzZXJuYW1lIjoidXNlcm5hbWUxIiwiZXh0cmFJbmZvIjoiZXh0cmFJbmZvMSJ9.LB1v6EUqiWveXfqS9UnMHiOdcxjx1wqwlp7gm8E5BxfUMTFTLZZYCjipDs17K34odkzLR0HvmDzewkoSZaE-rA";

    JwtFilterUtil jwtFilterUtil = new JwtFilterUtil();

//    assertEquals(false,jwtFilterUtil.isTokenValidAndFresh(invalidToken));
//    assertEquals(false,jwtFilterUtil.isTokenValidAndFresh(validExpiredToken));
    assertEquals(true,jwtFilterUtil.isTokenValidAndFresh(validFreshToken));

  }




}
