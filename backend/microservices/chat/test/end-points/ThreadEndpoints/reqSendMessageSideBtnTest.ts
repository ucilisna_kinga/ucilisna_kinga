import * as ThreadEndPoints from './../../../src/end-points/ThreadEndPoint';
import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import {User} from '../../../src/models/db/User';
import {Thread} from '../../../src/models/db/Thread';
import * as db_test from '../../config/db_test_connection';
import * as ThreadService from '../../../src/service/ThreadService';



export function reqSendMessageSideBtnTest() {
  const freshTablesCreated = SetupQueriesActions.freshTablesCreated();


  const threadsCreated = freshTablesCreated.then( () => {
    const user1: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user2: User = new User('studentFemale', 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg');

    const thread1: Thread = new Thread('threadId_1', ((new Date()).getTime()).toString(), 'last message text');
    ThreadService.createThread(db_test.client, thread1, [user1, user2]);

    const user3: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user4: User = new User('teacherFemale', 'Учителка Име', 'Учителка Презиме', 'teacher_female.jpg');

    const thread2: Thread = new Thread('threadId_2', ((new Date()).getTime()).toString(), 'last message text 2');
    ThreadService.createThread(db_test.client, thread2, [user3, user4]);
  });

  threadsCreated.then( () => {
    const senderObj = {
      username: 'teacherMale',
      firstname: 'Учител Име',
      lastname: 'Учител Презиме',
      avatarUrl: 'teacher_male.jpg'
    };
    const receiverObj = {
      username: 'studentFemale',
      firstname: 'Ученичка Име',
      lastname: 'Ученичка Презиме',
      avatarUrl: 'student_female.jpg'
    };
    const data = {sender: senderObj, receiver: receiverObj};
    ThreadEndPoints.reqSendMessageSideBtn(data);
  })
  .then(() => {
    SetupQueriesActions.tablesDeleted().then(() => console.log('tables deleted  '));
  });
}

reqSendMessageSideBtnTest();
