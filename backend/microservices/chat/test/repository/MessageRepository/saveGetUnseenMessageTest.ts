
import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import * as MessageRepository from '../../../src/repository/MessageRepository';
import * as db_test from '../../config/db_test_connection';


export function saveGetUnseenMessageTest() {
  SetupQueriesActions.freshTablesCreated()
    .then(() => {
      // given
      // when
      return MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'false');
      // then
    })
    .then(() => {
      console.log('unseen message saved');
      return MessageRepository.getAllUnseenMessageByUsername(db_test.client, 'teacherMale', 'false');
    })
    .then((data) => {
      console.log('getAllUnseenMessageByUsername result:');
      console.log(data.rows);
      return MessageRepository.getAllUnseenMessageByThreadId(db_test.client, 'thread_id_1', 'false');
    })
    .then((data) => {
      console.log('getAllUnseenMessageByThreadId result:');
      console.log(data.rows);
      return MessageRepository.getAllUnseenMessageByUsernameAndThreadId(db_test.client, 'teacherMale', 'thread_id_1', 'false');
    })
    .then((data) => {
      console.log('getAllUnseenMessageByUsernameAndThreadId result: ');
      console.log(data.rows);
      return SetupQueriesActions.tablesDeleted().then(() => console.log('tables deleted  '));
    });


}
saveGetUnseenMessageTest();

