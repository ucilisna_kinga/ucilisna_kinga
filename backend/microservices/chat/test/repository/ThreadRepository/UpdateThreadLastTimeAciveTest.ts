import * as td from '../../db/TablesDefinitions';
import * as db_test from '../../config/db_test_connection';
import {User} from '../../../src/models/db/User';
import {Thread} from '../../../src/models/db/Thread';
import * as ThreadService from '../../../src/service/ThreadService';
import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import * as ThreadRepository from '../../../src/repository/ThreadRepository';



export function updateThreadLastTimeActive() {
  SetupQueriesActions.freshTablesCreated()
    .then( () => {
      const oldTime = '1499004988205';
      const thread1: Thread = new Thread('threadId_1', oldTime , 'last message text');
      const user1: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
      ThreadRepository.saveThread(db_test.client, thread1, user1)
        .then( () => {
          return ThreadRepository.updateThreadTime(db_test.client, user1.username, thread1)
            .then( () => ThreadRepository.getThreadsByUsername(db_test.client, user1.username ));
        })
        .then( (updatedThread) => {
          console.log('updatedThread');
          console.log(updatedThread.rows[0]);
          SetupQueriesActions.tablesDeleted();
        })
        .then( () => console.log('tables deleted') );

    } );



}

updateThreadLastTimeActive();
