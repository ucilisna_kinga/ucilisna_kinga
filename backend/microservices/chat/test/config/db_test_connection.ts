const cassandra = require('cassandra-driver');
export const client = new cassandra.Client({ contactPoints: ['127.0.0.1:9042'], keyspace: 'chat_test' });
export const uuid = require('uuid/v4');
