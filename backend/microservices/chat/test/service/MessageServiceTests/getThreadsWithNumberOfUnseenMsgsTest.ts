import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import * as MessageRepository from '../../../src/repository/MessageRepository';
import * as MessageService from '../../../src/service/MessageService';
import * as db_test from '../../config/db_test_connection';


export function getThreadsWithNumberOfUnseenMsgsTest() {
  SetupQueriesActions.freshTablesCreated()
    .then(() => {
      // given
      return Promise.all([
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'false'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'false'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'true'),
        MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'false'),
      ]);

    })
    .then(() => {
      // when
      return MessageService.getThreadsWithNumberOfUnseenMsgs(db_test.client, 'teacherMale');
    })
    .then((data) => {
      // then
      console.log('data');
      console.log(data);
      return SetupQueriesActions.tablesDeleted().then(() => console.log('tables deleted  '));
    });


}
getThreadsWithNumberOfUnseenMsgsTest();

