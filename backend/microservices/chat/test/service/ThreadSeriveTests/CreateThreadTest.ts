import * as td from '../../db/TablesDefinitions';
import * as db_test from '../../config/db_test_connection';
import {User} from '../../../src/models/db/User';
import {Thread} from '../../../src/models/db/Thread';
import * as ThreadService from '../../../src/service/ThreadService';
import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import * as ThreadRepository from '../../../src/repository/ThreadRepository';


export function createThreadTest() {

  const freshTablesCreated = SetupQueriesActions.freshTablesCreated();

  const threadCreated = freshTablesCreated.then( () => {
    const user1: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user2: User = new User('studentFemale', 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg');

    const thread1: Thread = new Thread('threadId_1', ((new Date()).getTime()).toString(), 'last message text');
    return ThreadService.createThread(db_test.client, thread1, [user1, user2]);
  });

  threadCreated.then( (data) => {
    console.log('data : ');
    console.log(data);
    SetupQueriesActions.tablesDeleted().then( () => console.log('data deleted') );
  });

}
createThreadTest();
