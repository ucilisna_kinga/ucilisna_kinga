import * as db_test from '../../config/db_test_connection';
import {User} from '../../../src/models/db/User';
import {Thread} from '../../../src/models/db/Thread';
import * as ThreadService from '../../../src/service/ThreadService';
import * as SetupQueriesActions from '../../db/SetupQueriesActions';

export function threadExistForTwoUsersTest() {

  /**
   threads by user
   teacherMale   threadId_1  threadLastTimeActive threadLastMessage
   studentFemale threadId_1  threadLastTimeActive threadLastMessage
   users_by_thread
   threadId_1  teacherMale  'Учител Име', 'Учител Презиме', 'teacher_male.jpg'
   threadId_1  studentFemale 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg'
   */
  const freshTablesCreated = SetupQueriesActions.freshTablesCreated();

  const threadsCreated = freshTablesCreated.then( () => {
    const user1: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user2: User = new User('studentFemale', 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg');

    const thread1: Thread = new Thread('threadId_1', ((new Date()).getTime()).toString(), 'last message text');
    return ThreadService.createThread(db_test.client, thread1, [user1, user2]);
  });

  threadsCreated.then( () => {
    const sender = 'teacherMale';
    const receiver = 'studentFemale';
    ThreadService.threadExistForSenderAndReceiver(db_test.client, sender, receiver)
      .then( (thread_id) => {
        console.log('thread exist result:: ');
        console.log(thread_id);

        SetupQueriesActions.tablesDeleted()
          .then( () => console.log('data deleted') );
      });
  });

}

threadExistForTwoUsersTest();
