import {Thread} from '../../src/models/db/Thread';
import {uuid} from '../config/db_test_connection';

function threadTestMain() {

  let threadLastTimeActive: String;
  let threadId: String;
  let t1: Thread;

  function createThread() {
    threadLastTimeActive = ((new Date()).getTime()).toString();
    threadId = uuid();
    t1 = new Thread('teacherMal', threadLastTimeActive, threadId, 'текст од последна пуштена порака' );
  }

  function getThreadTest() {
    console.log('get Username:');
    console.log(t1.username);
  }

  function setThreadTest() {
    console.log('set Username:');
    t1.username = 'teacherMale';
    console.log(t1.username);
  }

  createThread();
  getThreadTest();
  setThreadTest();
}

threadTestMain();
