"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function saveThread(client, t, u) {
    var query = "Insert into threads_by_user\n  ( username, thread_id, thread_last_time_active, last_message_text ) values(\n  '" + u.username + "', '" + t.thread_id + "', '" + t.thread_last_time_active + "', '" + t.last_message_text + "' ); ";
    return client.execute(query);
}
exports.saveThread = saveThread;
function getThreadsByUsername(client, username) {
    var query = "select * from threads_by_user where username = '" + username + "' ";
    return client.execute(query);
}
exports.getThreadsByUsername = getThreadsByUsername;
function getThreadById(client, username, thread_id) {
    var query = "select * from threads_by_user where username = '" + username + "' AND thread_id = '" + thread_id + "'  ";
    return client.execute(query);
}
exports.getThreadById = getThreadById;
function updateThreadTime(client, username, thread) {
    var newDate = (new Date()).getTime().toString();
    var query = "update threads_by_user SET thread_last_time_active = '" + newDate + "'\n   where username = '" + username + "' AND thread_id = '" + thread.thread_id + "'  ";
    return client.execute(query);
}
exports.updateThreadTime = updateThreadTime;
function updateThreadLastMsg(client, username, thread) {
    var newDate = (new Date()).getTime().toString();
    var query = "update threads_by_user SET last_message_text = '" + thread.last_message_text + "'\n   where username = '" + username + "' AND thread_id = '" + thread.thread_id + "'  ";
    return client.execute(query);
}
exports.updateThreadLastMsg = updateThreadLastMsg;
//# sourceMappingURL=ThreadRepository.js.map