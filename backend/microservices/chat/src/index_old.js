var express = require('express');

var chat_app = express();
var http = require('http').Server(chat_app);
var io = require('socket.io')(http);
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({ contactPoints: ['127.0.0.1:9042'], keyspace: 'user' });

var sampleThreads = [
  {
    id: 0,
    forUser: { username: 'jshalamanoski', firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  } ,
    participants: [
      { username: 'teacherFemale' , firstName: 'УчителкаИме', lastName: 'УчителкаПрезиме', gender: 'female' , profileType: 'teacher'  }
    ],
    relatedThreads: [1]
  },
  {
    id: 1,
    forUser: {username: 'teacherFemale', firstName: 'УчителкаИме', lastName: 'УчителкаПрезиме', gender: 'female', profileType: 'teacher'},
    participants: [
      { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  }
    ],
    relatedThreads: [0]
  },
  {
    id: 2,
    forUser: { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  } ,
    participants: [
      { username: 'studentFemale' , firstName: 'УченичкаИме', lastName: 'УченичкаПрезиме' , gender: 'female', profileType: 'student'  } ,
    ],
    relatedThreads: [3]
  },
  {
    id: 3,
    forUser: {username: 'studentFemale', firstName: 'УченичкаИме', lastName: 'УченичкаПрезиме', gender: 'female', profileType: 'student'},
    participants: [
      { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  }
    ],
    relatedThreads: [2]
  }
];

function sendUserByUsername(username) {
  var user = null;
  var user_id = username;
  // var user_id = "'e13637c1-2fde-412b-a081-daa3d913e558'";
  var query = "select * from user.users where user_id = " + user_id;
  client.execute(query,function(err, result) {
    console.log(result.rows[0]);
    socket.emit('res-get-all-chat-threads', result.rows[0]);
  });

}


io.on('connection', function (socket) {

  console.log('Socket connection established');

  socket.on('disconnect', function () {
    console.log('Socket connection Disconnected');
  });

  socket.on('req-get-all-chat-threads', function (data) {
    sendUserByUsername("'e13637c1-2fde-412b-a081-daa3d913e558'");
  });

  function sendUserByUsername(username) {
    var user_id = username;
    // var user_id = "'e13637c1-2fde-412b-a081-daa3d913e558'";
    var query = "select * from user.users where user_id = " + user_id;
    client.execute(query,function(err, result) {
      console.log(result.rows[0]);
      socket.emit('res-get-all-chat-threads', result.rows[0]);
    });
  }

});


http.listen(3000, function () {
  console.log("App started listening on port 3000");

});
