import * as db from './../repository/db_connection';
// import * as db from './../../test/config/db_test_connection';
import {ThreadWithUsers} from '../models/dto/ThreadWithUsers';
import {User} from '../models/db/User';
import * as dtpConverter from './../models/utilities/dtoConvertors/UserToUserDto';
import * as ThreadService from '../service/ThreadService';
import * as UserService from './../service/UserService';
import * as UserRepository from '../repository/UserRepository';
import {io} from './../index';
import {convertRawDbUsersToUsersDto} from '../models/utilities/dtoConvertors/UserToUserDto';



export let UserEndPoint = function (app, socket) {
  this.app = app;
  this.socket = socket;

  // Expose handler methods for events
  this.handler = {
    reqGetAllActiveUsers: reqGetAllActiveUsers.bind(this),
    reqInsertFriendship: reqInsertFriendship.bind(this)
  };

  function reqGetAllActiveUsers(activeUserUsername) {
    // get all sockets
    const allSockets = io.sockets.sockets;
    // list all sockets and get username for sockets
    UserRepository.getAllUserFriends(db.client, activeUserUsername) // create this method in user repo
      .then(userFriends => {
        const activeFriends = [];
        userFriends.rows.forEach( uf => {
          allSockets.forEach( s => {
            if ( uf.username === s.handshake.query.username ) {
              activeFriends.push(uf);
            }
          });
        });
        this.socket.emit('resGetAllActiveUsers', convertRawDbUsersToUsersDto(activeFriends) );
      });
  }

  function reqInsertFriendship(users) {
    // friend, user
    users.friend.firstname = users.friend.firstName;
    users.friend.lastname = users.friend.lastName;
    users.user.firstname = users.user.firstName;
    users.user.lastname = users.user.lastName;
    UserService.insertUserFirendship(db.client, users.friend, users.user)
      .then(() => {
        // resInsertFriendship
      });
    // console.log('users');
    // console.log(users);
  }

};
