import * as ThreadRepository from './../repository/ThreadRepository';
import * as db from './../repository/db_connection';
// import * as db from './../../test/config/db_test_connection';
import {ThreadWithUsers} from '../models/dto/ThreadWithUsers';
import {User} from '../models/db/User';
import * as dtpConverter from './../models/utilities/dtoConvertors/UserToUserDto';
import * as ThreadService from '../service/ThreadService';
import * as MessageService from '../service/MessageService';
import {Thread} from '../models/db/Thread';
import {uuid} from '../repository/db_connection';

export let ThreadEndPoint = function (app, socket) {
  this.app = app;
  this.socket = socket;

  // Expose handler methods for events
  this.handler = {
    reqGetAllThreadsByUsername:    reqGetAllThreadsByUsername.bind(this),    // and this.socket in events
    reqGetAllThreadsByUsernameForUpdate:    reqGetAllThreadsByUsernameForUpdate.bind(this),    // and this.socket in events
    reqSendMessageSideBtn:    reqSendMessageSideBtn.bind(this)    // and this.socket in events
  };
};

// Events
export function reqGetAllThreadsByUsername(data) {

  const socket = this.socket;

  const threadWithUsersSorted =  ThreadService.getThreadsByUsername(db.client, data.username)
    .then( threadsWithUsers => {
      const sortedThreadsWithUsers = threadsWithUsers.sort( function(a, b){
        return b.thread_last_time_active - a.thread_last_time_active;
      });
      // test
      return Promise.resolve(sortedThreadsWithUsers);
    });

  const finalReturnArray = Promise.all([
    MessageService.getThreadsWithNumberOfUnseenMsgs(db.client, data.username),
    threadWithUsersSorted
  ]).then( dataPromiseAll => {
    const threadsWithUnseenMsgs = dataPromiseAll[0];
    for (const t_id in threadsWithUnseenMsgs) {
      // console.log(t_id);
      // console.log(threadsWithUnseenMsgs[t_id]);
      dataPromiseAll[1].forEach( twusers => {
       if (twusers.thread_id === t_id) {
         twusers.numOfUnseenMessagesForActiveUser = threadsWithUnseenMsgs[t_id];
       }
      });
    }
    return Promise.resolve(dataPromiseAll[1]);
  });

  finalReturnArray.then((returnData) => socket.emit('resGetAllThreadsByUsername', returnData));
  // return for test only
  // return finalReturnArray;
}

export function reqGetAllThreadsByUsernameForUpdate(data) {

  const socket = this.socket;

  const threadWithUsersSorted =  ThreadService.getThreadsByUsername(db.client, data.username)
    .then( threadsWithUsers => {
      const sortedThreadsWithUsers = threadsWithUsers.sort( function(a, b){
        return b.thread_last_time_active - a.thread_last_time_active;
      });
      // test
      return Promise.resolve(sortedThreadsWithUsers);
    });

  const finalReturnArray = Promise.all([
    MessageService.getThreadsWithNumberOfUnseenMsgs(db.client, data.username),
    threadWithUsersSorted
  ]).then( dataPromiseAll => {
    const threadsWithUnseenMsgs = dataPromiseAll[0];
    for (const t_id in threadsWithUnseenMsgs) {
      // console.log(t_id);
      // console.log(threadsWithUnseenMsgs[t_id]);
      dataPromiseAll[1].forEach( twusers => {
        if (twusers.thread_id === t_id) {
          twusers.numOfUnseenMessagesForActiveUser = threadsWithUnseenMsgs[t_id];
        }
      });
    }
    return Promise.resolve(dataPromiseAll[1]);
  });

  finalReturnArray.then((returnData) => socket.emit('resGetAllThreadsByUsernameForUpdate', returnData));
}

export function reqSendMessageSideBtn(data) {
  let commonThread = null;

  const commonThreadReturn = ThreadService.threadExistForSenderAndReceiver(db.client, data.sender.username, data.receiver.username)
    .then((thread_id) => {
      if (thread_id === 'false') {
        commonThread = new Thread( '', uuid(), (new Date()).getTime().toString(), '');
        // create users with User constructor from data.sender and data.receiver
        return ThreadService.createThread(db.client, commonThread , [data.sender, data.receiver]);
      } else {
        return ThreadService.getThreadById(db.client, data.sender.username, thread_id)
          .then( (thread) =>  {
            return Promise.resolve(thread);
          })
          .then( (thread) => {
            const threadData = thread.rows[0];
            return ThreadService.updateThreadTimeAndLastMsgAllThreadUsers(db.client, data.sender.username, threadData )
              .then( () => { return ThreadService.getThreadById(db.client, data.sender.username, thread_id); })
              .then( (threadRaw) => threadRaw.rows[0] );
          });
      }
    });

  commonThreadReturn.then( (commonThreadEmit) => {
      this.socket.emit('resSendMessageSideBtn', { done: true, commonThread: commonThreadEmit} );
  });
}
