
export class Message {

  public id: string;
  public thread_id: string;
  public text: string;
  public time_created: string;
  public created_by_username: string;


  constructor(id: string, thread_id: string, text: string, time_created: string, created_by_username: string) {
    this.id = id;
    this.thread_id = thread_id;
    this.text = text;
    this.time_created = time_created;
    this.created_by_username = created_by_username;
  }
}
