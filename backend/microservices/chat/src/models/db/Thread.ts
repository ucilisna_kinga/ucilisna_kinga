export class Thread {

  private _username: String;
  private _thread_id: String;
  private _thread_last_time_active: String;
  private _last_message_text: String;

  constructor(username: String, thread_id: String, thread_last_time_active: String, last_message_text: String) {
    this._username = username;
    this._thread_id = thread_id;
    this._thread_last_time_active = thread_last_time_active;
    this._last_message_text = last_message_text;
  }

  get username(): String {
    return this._username;
  }

  set username(value: String) {
    this._username = value;
  }

  get thread_id(): String {
    return this._thread_id;
  }

  set thread_id(value: String) {
    this._thread_id = value;
  }

  get thread_last_time_active(): String {
    return this._thread_last_time_active;
  }

  set thread_last_time_active(value: String) {
    this._thread_last_time_active = value;
  }

  get last_message_text(): String {
    return this._last_message_text;
  }

  set last_message_text(value: String) {
    this._last_message_text = value;
  }
}
