import {UserDto} from './UserDto';
export class ThreadWithUsers {

  public username;
  public thread_last_time_active;
  public thread_id;
  public users: Array<UserDto> = [];
  public last_message_text;
  public numOfUnseenMessagesForActiveUser;


  constructor(username, thread_last_time_active, thread_id, users: Array<UserDto>, last_message_text) {
    this.username = username;
    this.thread_last_time_active = thread_last_time_active;
    this.thread_id = thread_id;
    this.users = users;
    this.last_message_text = last_message_text;
    this.numOfUnseenMessagesForActiveUser = 0;
  }



}
