
export class MessageDto {

  public id: string;
  public threadId: string;
  public text: string;
  public timeCreated: string;
  public sendByUsername: string;

  constructor(id: string, threadId: string, text: string, timeCreated: string, sendByUsername: string) {
    this.id = id;
    this.threadId = threadId;
    this.text = text;
    this.timeCreated = timeCreated;
    this.sendByUsername = sendByUsername;
  }
}
