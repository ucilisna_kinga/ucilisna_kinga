import {MessageDto} from '../../dto/MessageDto';

export function messagesToMessagesDtoAndSort(messages) {
  const messagesDto = [];

  messages.forEach( m => {
    const mDto = new MessageDto(m.id, m.thread_id, m.text, m.time_created, m.created_by_username);
    messagesDto.push(mDto);
  });
  const sortedMessagesDto = sortMessagesByTimeCreated(messagesDto);
  return sortedMessagesDto;
}

export function sortMessagesByTimeCreated (messages) {
  return messages.sort( function(a, b){
    return a.timeCreated - b.timeCreated;
  });
}

