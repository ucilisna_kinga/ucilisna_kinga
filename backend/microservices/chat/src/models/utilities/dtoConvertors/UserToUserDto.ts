import {User} from '../../db/User';
import {UserDto} from '../../dto/UserDto';

export function convertUsersToUsersDto(users: Array<User>): Array<UserDto> {
  const usersDto: Array<UserDto> = [];

  users.forEach( u => {
    const udto = new UserDto(u.username, u.firstname, u.lastname, u.avatarUrl);
    usersDto.push(udto);
  });

  return usersDto;
}

export function convertRawDbUsersToUsersDto(users): Array<UserDto> {
  const usersDto: Array<UserDto> = [];

  users.forEach( u => {
    const udto = new UserDto(u.username, u.first_name, u.last_name, u.avatar_url);
    usersDto.push(udto);
  });

  return usersDto;
}
