export let UserRestController = function init(chat_app) {

  chat_app.get('/user', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send({username: 'jshalamanoski-works'});
  });

};
