
/**
 * tmp data
 * username1 thread1  1111112222333  lasttextmessage
 * username1 thread2  1111112222333  lasttextmessage
 * username1 thread3  1111112222333  lasttextmessage
 * username1 thread4  1111112222333  lasttextmessage
 *
 * username2 thread1  1111112222333  lasttextmessage
 * username2 thread2  1111112222333  lasttextmessage
 * username2 thread3  1111112222333  lasttextmessage
 * username2 thread4  1111112222333  lasttextmessage
 *
 */
export const threads_by_user = `
  CREATE TABLE IF NOT EXISTS threads_by_user (
    username text,
    thread_id text,
    thread_last_time_active text,
    last_message_text text,
    PRIMARY KEY (username, thread_id)
  )`;

/**
 * tmp data users_by_thread
 *
 * thread1  username1   firstname1 lastname1
 * thread1  username2   firstname2 lastname2
 * thread1  username3   firstname3 lastname3
 *
 * thread2  username3   firstname3 lastname3
 * thread2  username4   firstname4 lastname4
 * thread2  username5   firstname5 lastname5
 *
 */
export const users_by_thread = `
  CREATE TABLE IF NOT EXISTS users_by_thread (
    thread_id text,
    username text,
    first_name text,
    last_name text,
    avatar_url text,
    PRIMARY KEY (thread_id, username)
  )`;

export const messages_by_thread = `
  CREATE TABLE IF NOT EXISTS messages_by_thread (
    id text,
    thread_id text,
    text text,
    time_created text,
    created_by_username text,
    PRIMARY KEY (thread_id, id)
  );`;

/**
 *  tmp data
 *
 *  id1  thread1    user1   true
 *  id2  thread1    user2   true
 *  id2  thread1    user2   false
 */

export const unseen_messages = `
  CREATE TABLE IF NOT EXISTS unseen_messages (
    id text,
    thread_id text,
    username text,
    seen text,
    PRIMARY KEY (id)
  );`;

// two inserts are needed for one friendship
// username friendForUsername ...
// user1    user2
// user2    user1

// check for friendship -- two positive queries   // username:user1 friendForUsername: user2  and  the opposite one

export const user_friends = `
  CREATE TABLE IF NOT EXISTS user_friends (
    id text,
    username text,
    friend_for_username text,
    first_name text,
    last_name text,
    avatar_url text,
    PRIMARY KEY (friend_for_username, username  )
  );`;
