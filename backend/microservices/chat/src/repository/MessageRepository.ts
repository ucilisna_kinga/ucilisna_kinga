import {Message} from '../models/db/Message';
import {uuid} from './db_connection';



export function saveMessage(client, message: Message ) {
  const q = ` Insert into messages_by_thread 
  (id, thread_id, text, time_created, created_by_username) values
  ('${message.id}', '${message.thread_id}', '${message.text}', '${message.time_created}', '${message.created_by_username}');
  `;
  return client.execute(q);
}

export function getAllMessagesByThreadId(client, threadId) {
  const q = `select * from messages_by_thread where thread_id = '${threadId}';`;
  return client.execute(q);
}





// UnSeenMessages


export function saveUnseenMessage(client, thread_id, username, seen) {
  const newUUID = uuid();
  const q = `
    insert into unseen_messages
      (id, thread_id, username, seen) values
      ('${newUUID}','${thread_id}','${username}','${seen}');
  `;
  return client.execute(q);
}

export function updateUnseenMessageById(client, id, seen) {
  const q = `Update unseen_messages set seen='${seen}' where id='${id}';`;
  return client.execute(q);
}

export function getAllUnseenMessageByUsername(client, username, seen) {
  const q = `select * from unseen_messages where username = '${username}' and seen='${seen}' ALLOW FILTERING;`;
  return client.execute(q);
}

export function getAllUnseenMessageByThreadId(client, thread_id, seen) {
  const q = `select * from unseen_messages where thread_id = '${thread_id}' and seen='${seen}' ALLOW FILTERING;`;
  return client.execute(q);
}

export function getAllUnseenMessageByUsernameAndThreadId(client, username, thread_id, seen) {
  const q = `select * from unseen_messages where 
  username = '${username}' 
  and thread_id = '${thread_id}' 
  and seen='${seen}' ALLOW FILTERING;`;
  return client.execute(q);
}
