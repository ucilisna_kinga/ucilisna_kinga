import * as TablesDefinitions from './TablesDefinitions';
import {friendshipSetupDataDatabase} from './FriendhipSetupDataDatabase';

let dbIpAddress = '127.0.0.1:9042';
let keySpace = 'chat';
if (process.argv[2] === 'prod') {
  dbIpAddress = '178.238.235.229:9042';
} else if (process.argv[2] === 'stage' ) {
  dbIpAddress = '178.238.235.229:9042';
  keySpace = 'chat_stage';
}
console.log('arg:', process.argv[2]);

const cassandra = require('cassandra-driver');
export const client = new cassandra.Client({ contactPoints: [dbIpAddress], keyspace: keySpace });

export const uuid = require('uuid/v4');

export function createAllTables() {
  Promise.all(
    [ client.execute(TablesDefinitions.threads_by_user),
      client.execute(TablesDefinitions.users_by_thread),
      client.execute(TablesDefinitions.messages_by_thread),
      client.execute(TablesDefinitions.unseen_messages),
      client.execute(TablesDefinitions.user_friends)
    ]).then(() => {
      console.log('tables created');
      friendshipSetupDataDatabase().then( () => console.log('friendship data inserted '));
  });
}

createAllTables();
