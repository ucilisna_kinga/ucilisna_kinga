import {Thread} from '../models/db/Thread';
import {User} from '../models/db/User';
import {uuid} from "./db_connection";

export function getUsersByThread(client, thread: Thread) {
  const query = ` Select * from users_by_thread where thread_id = '${thread.thread_id}';`;

  return client.execute(query);
}

export function saveUser(client, t: Thread, u: User) {
  const query = ` Insert into users_by_thread
  ( thread_id, username, first_name, last_name, avatar_url) values
  ( '${t.thread_id}','${u.username}','${u.firstname}','${u.lastname}','${u.avatarUrl}' );  `;

  return client.execute(query);
}

export function getAllUsers(client) {
  const query = `select * from users_by_thread`;
  return client.execute(query);
}


export function saveUserFriend(client, user, friendForUsername) {
  const q = `
    Insert into user_friends 
    ( id, username, friend_for_username , first_name, last_name, avatar_url) values
    ( '${uuid()}', '${user.username}', '${friendForUsername}', '${user.firstname}', '${user.lastname}', '${user.avatarUrl}' );
  `;
  return client.execute(q);
}

export function getUserFromFriends(client, username, friendForUsername) {
  const q = `Select * from user_friends where friend_for_username = '${friendForUsername}' and username='${username}' `;
  return client.execute(q);
}

export function getAllUserFriends(client, username) {
  const q = `Select * from user_friends where friend_for_username='${username}'`;
  return client.execute(q);
}

export function deleteAllUserFriends(client) {
  const q = `truncate user_friends;`;
  return client.execute(q);
}


