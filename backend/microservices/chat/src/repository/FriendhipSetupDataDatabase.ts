import * as UserService from './../service/UserService';
import * as UserRepository from './../repository/UserRepository';
import * as db from './../repository/db_connection';
import {User} from '../models/db/User';

export function friendshipSetupDataDatabase() {

  const jshalamanoski = new User('jshalamanoski', 'Јован', 'Шаламаноски', 'teacher_male.jpg');
  const teacherFemale = new User('teacherFemale', 'Учителка Име', 'Учителка Презиме', 'teacher_female.jpg');
  const studentMale = new User('studentMale', 'Ученик Име', 'Ученик Презиме', 'student_male.jpg');
  const studentFemale = new User('studentFemale', 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg');

  const parentMale = new User('parentMale', 'Родител Име', 'Родител Презиме', 'parent_male.jpg');
  const parentFemale = new User('parentFemale', 'Родителка Име', 'Родителка Презиме', 'parent_female.jpg');

  return UserRepository.deleteAllUserFriends(db.client).then( () => {
    Promise.all([
      UserService.insertUserFirendship(db.client, jshalamanoski, teacherFemale),
      UserService.insertUserFirendship(db.client, jshalamanoski, studentMale),
      UserService.insertUserFirendship(db.client, jshalamanoski, studentFemale),
      // UserService.insertUserFirendship(db.client, jshalamanoski, parentMale),
      // UserService.insertUserFirendship(db.client, jshalamanoski, parentFemale),

      UserService.insertUserFirendship(db.client, teacherFemale, studentMale),
      UserService.insertUserFirendship(db.client, teacherFemale, studentFemale),
      // UserService.insertUserFirendship(db.client, teacherFemale, parentMale),
      // UserService.insertUserFirendship(db.client, teacherFemale, parentFemale),

      UserService.insertUserFirendship(db.client, studentMale, studentFemale),
      // UserService.insertUserFirendship(db.client, studentMale, parentMale),
      // UserService.insertUserFirendship(db.client, studentMale, parentFemale),

      // UserService.insertUserFirendship(db.client, studentFemale, parentMale),
      // UserService.insertUserFirendship(db.client, studentFemale, parentFemale),

      // UserService.insertUserFirendship(db.client, parentMale, parentFemale)
    ]);
  });
}

