import {User} from '../models/db/User';
import * as UserRepository from './../repository/UserRepository';

export function insertUserFirendship(client, user1: User, user2: User) {
  return Promise.all([
    UserRepository.saveUserFriend(client, user1, user2.username),
    UserRepository.saveUserFriend(client, user2, user1.username)
  ]);
}
