import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AppComponent } from './app.component';
import {AppEffectsRun} from 'app/store/effects/app-effects';
import {reducer} from './store/reducers/index';
import {RouterModule} from '@angular/router';
import {routes} from '../app/core/router/routes';
import {StoreModule} from '@ngrx/store';
import {RouterStoreModule } from '@ngrx/router-store';
import {StoreDevtoolsModule } from '@ngrx/store-devtools';
import {BookTmpService} from './store/data-services/book-tmp-service';
import {BookHomePageComponent} from './routed-components/_book/book-home-page/book-home-page.component';
import {MyCollectionsPageComponent} from './routed-components/_book/my-collections-page/my-book-collections-page.component';
import {SearchBookComponent} from './modules/books/views/search-book/search-book.component';
import {BookNavbarComponent} from './modules/books/views/navbar/book-navbar.component';
import {BookCollectionsGridComponent} from './modules/books/views/book-collections-grid/book-collections-grid.component';
import {ProfileWallPageComponent } from './routed-components/profile-wall-page/profile-wall-page.component';
import {LoginComponent} from './modules/header/views/login/login.component';
import {HeaderComponent} from './modules/header/container/header.component';
import {AuthTmpService} from 'app/core/services/auth/auth-tmp-service';
import {SingInPageComponent} from './routed-components/sing-in-page/sing-in-page.component';
import {MiddleIconsComponent} from 'app/modules/header/views/middle-icons/middle-icons.component';
import {NgbDropdownModule} from './modules/ng-bootstrap/dropdown/dropdown.module';
import {NgbTypeaheadModule} from './modules/ng-bootstrap/typeahead/typeahead.module';
import {NgbTypeaheadConfig} from './modules/ng-bootstrap/typeahead/typeahead-config';
import {NgbDropdownConfig} from './modules/ng-bootstrap/dropdown/dropdown-config';
import {RegisterComponent} from 'app/modules/register/register/register.component';
import {UserTmpService} from './store/data-services/user-tmp-service';
import {NgbAlertModule} from './modules/ng-bootstrap/alert/alert.module';
import {ProfileMainButtonsComponent } from './modules/sidebar/views/profile-main-buttons/profile-main-buttons.component';
import {ProfilePictureComponent } from './modules/sidebar/views/profile-picture/profile-picture.component';
import {SideBarContainerComponent} from './modules/sidebar/container/sidebar-container';
import {PostInstanceComponentComponent } from './modules/post/views/post-instance-component/post-instance-component.component';
import {PostCreateComponent} from './modules/post/views/post-create/post-create.component';
import {OnlineChatUserComponent } from './modules/chat/views/online-chat-user/online-chat-user.component';
import {ListOfOnlineChatUsersComponent } from './modules/chat/containers/list-of-online-chat-users/list-of-online-chat-users.component';
import {ProfileFriendsPageComponent } from './routed-components/profile-friends-page/profile-friends-page.component';
import {UserDisplayFriendsComponent } from './modules/friends/views/user-display-friends/user-display-friends.component';
import {ProfileNewsFeedPageComponent } from './routed-components/profile-news-feed-page/profile-news-feed-page.component';
import {ProfileMessagesPageComponent } from './routed-components/profile-messages-page/profile-messages-page.component';
import {ProfileDetailsPageComponent } from './routed-components/profile-details-page/profile-details-page.component';
import {PostTmpService} from './store/data-services/post-tmp-service';
import {ListChatThreadsComponent } from './modules/chat/views/list-chat-threads/list-chat-threads.component';
import {ThreadTmpService} from './store/data-services/thread-tmp-service';
import {MessageTmpService} from 'app/store/data-services/message-tmp-service';
import { ChatThreadInstanceComponent } from './modules/chat/views/chat-thread-instance/chat-thread-instance.component';
import { ChatMessageCenterComponent } from './modules/chat/views/chat-message-center/chat-message-center.component';
import { ChatMessageLineComponent } from './modules/chat/views/chat-message-line/chat-message-line.component';
import {ChatService} from './store/data-services/chat-service';
import { ChatMainContainerComponent } from './modules/chat/containers/chat-main-container/chat-main-container.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import {UserService} from './store/data-services/user.service';
import {PostService} from './store/data-services/post.service';
import {UiStateService} from './core/services/ui-state.service';
import { FriendsRequestsComponent } from './modules/header/views/friends-requests/friends-requests.component';

@NgModule({
  declarations: [
    AppComponent,
    SingInPageComponent,
    BookHomePageComponent,
    MyCollectionsPageComponent,
    BookCollectionsGridComponent,
    SearchBookComponent,
    BookNavbarComponent,
    ProfileWallPageComponent,
    LoginComponent,
    HeaderComponent,
    MiddleIconsComponent,
    RegisterComponent,
    ProfileMainButtonsComponent,
    ProfilePictureComponent,
    SideBarContainerComponent,
    PostCreateComponent,
    PostInstanceComponentComponent,
    OnlineChatUserComponent,
    ListOfOnlineChatUsersComponent,
    ProfileFriendsPageComponent,
    UserDisplayFriendsComponent,
    ProfileNewsFeedPageComponent,
    ProfileMessagesPageComponent,
    ProfileDetailsPageComponent,
    ListChatThreadsComponent,
    ChatThreadInstanceComponent,
    ChatMessageCenterComponent,
    ChatMessageLineComponent,
    ChatMainContainerComponent,
    SpinnerComponent,
    FriendsRequestsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbDropdownModule,
    NgbTypeaheadModule,
    NgbAlertModule.forRoot(),
    // NgbModule.forRoot(),
    RouterModule.forRoot(routes, { useHash: true }),
    StoreModule.provideStore(reducer),
    RouterStoreModule.connectRouter(),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    AppEffectsRun
  ],
  providers: [
    BookTmpService,
    AuthTmpService,
    NgbTypeaheadConfig,
    NgbDropdownConfig,
    UserTmpService,
    PostTmpService,
    MessageTmpService,
    ThreadTmpService,
    ChatService,
    UserService,
    PostService,
    UiStateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
