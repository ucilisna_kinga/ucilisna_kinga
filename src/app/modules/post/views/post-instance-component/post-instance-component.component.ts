import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-post-instance-component',
  templateUrl: './post-instance-component.component.html',
  styleUrls: ['./post-instance-component.component.css']
})
export class PostInstanceComponentComponent {

  @Input() wallUser;
  @Input() post;
  @Input() creatorOfPost;
  @Input() animation = false;
  constructor() { }

}
