import {Component, EventEmitter, Output} from '@angular/core';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {

  @Output() createdPost = new EventEmitter();
  postText = '';

  constructor() { }

  createPost(postText) {
    let haveEmptyNewLines = true;
    const lines = postText.split(/\r\n|\r|\n/g);

    for (const l of lines) {
      if (l.length > 0 ) {
        haveEmptyNewLines = false;
      }
    }

    if (postText.length > 0 && !haveEmptyNewLines) {
      this.createdPost.emit(postText);
      this.postText = '';
    }

  }
}
