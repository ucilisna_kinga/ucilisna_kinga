import {Component, Input, ChangeDetectionStrategy} from '@angular/core';
import {Book} from '../../../../store/models/db-models/book';

@Component({
  selector: 'app-book-collections-grid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './book-collections-grid.component.html',
  styleUrls: ['./book-collections-grid.component.css']
})
export class BookCollectionsGridComponent {
  @Input() books: Book;
}
