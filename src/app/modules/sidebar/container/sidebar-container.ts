import {Component, Input, OnInit} from '@angular/core';
import {AuthTmpService} from '../../../core/services/auth/auth-tmp-service';
import {Router, ActivatedRoute} from '@angular/router';
import {ChatService} from '../../../store/data-services/chat-service';
import {UserService} from '../../../store/data-services/user.service';

@Component({
  selector: 'app-sidebar-cont',
  templateUrl: 'sidebar-container.component.html',
})
export class SideBarContainerComponent implements OnInit {

  @Input() wallUser ;
  @Input() profileRoute = 'wall';
  activeUser;
  activeUserFriends = [];
  activeUserFriendRequests = [];
  wallUserFriendRequests = [];
  doneLoadingFriends = false;

  constructor ( private _authTmpService: AuthTmpService,
                private _router: Router,
                private route: ActivatedRoute,
                private userService: UserService,
                private _chatService: ChatService) {
  }

  ngOnInit() {
    this.activeUser = this._authTmpService.getActiveUser();

    this.route.params.subscribe(params => {
      console.log('ch5--');
      this.userService.getUserFriendsByUsername(this.activeUser.username)
        .toPromise().then( friends => {
        this.activeUserFriends = friends;
        this.userService.getFriendRequests(this.activeUser.username).toPromise().then(friendRequests => {
            this.activeUserFriendRequests = friendRequests;
            this.userService.getFriendRequests(this.wallUser.username).toPromise().then( wfrs => {
              this.wallUserFriendRequests = wfrs;
              this.doneLoadingFriends = true;
            });
        });

      });
    });
  }

  isActiveUser() {
    if (this.activeUser.username === this.wallUser.username) {
      return true;
    } else {
      return false;
    }
  }

  onSendMessage() {
    const senderObj = {
      username: this.activeUser.username,
      firstname: this.activeUser.firstName,
      lastname: this.activeUser.lastName,
      avatarUrl: this.activeUser.avatarUrl
    };
    const receiverObj = {
      username: this.wallUser.username,
      firstname: this.wallUser.firstName,
      lastname: this.wallUser.lastName,
      avatarUrl: this.wallUser.avatarUrl
    };
    const data = {sender: senderObj, receiver: receiverObj};
    console.log('data');
    console.log(data);
    console.log('wallUser');
    console.log(this.wallUser);

    this._chatService.socket.emit('reqSendMessageSideBtn', data);
    this._router.navigate(['/', this.activeUser.username, 'messages']);
  }


  areFriends() {
    let result = false;
    if (!this.isActiveUser()) {
      this.activeUserFriends.forEach(f => {
        if (f.username === this.wallUser.username) {
          result = true;
        }
      });
      this.activeUserFriendRequests.forEach( fr => {
        if (fr.username === this.wallUser.username) {
          result = true;
        }
      });
      this.wallUserFriendRequests.forEach( wfr => {
        console.log(this.activeUser.username);
        if (wfr.username === this.activeUser.username) {
          result = true;
        }
      });
    }
    return result;

  }

  onAddFriend(user) {
    this.userService.addFriend(user.username)
      .subscribe( data => console.log(data));
  }

}
