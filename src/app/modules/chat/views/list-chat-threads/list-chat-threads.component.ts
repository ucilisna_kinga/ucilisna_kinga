import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-list-chat-threads',
  templateUrl: './list-chat-threads.component.html',
  styleUrls: ['./list-chat-threads.component.css']
})
export class ListChatThreadsComponent {

  @Input() userThreads;
  @Input() activeThread;
  @Input() activeUser;

  @Output() threadChanged = new EventEmitter();
  constructor() {
  }

  isActiveThread(testThread) {
    if (testThread.thread_id === this.activeThread.thread_id) {
      return 1;
    } else {
      return 0;
    }
  }

  getLastMessage(t) {
    if (this.isActiveThread(t)) {
      return this.activeThread.last_message_text;
    } else {
      return t.last_message_text;
    }
  }

  threadIsChanged(thread) {
    this.threadChanged.emit(thread);
  }

}
