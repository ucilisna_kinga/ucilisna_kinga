import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChatThreadsComponent } from './list-chat-threads.component';

describe('ListChatThreadsComponent', () => {
  let component: ListChatThreadsComponent;
  let fixture: ComponentFixture<ListChatThreadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListChatThreadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListChatThreadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
