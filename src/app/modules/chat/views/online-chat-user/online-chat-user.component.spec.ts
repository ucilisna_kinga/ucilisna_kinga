import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineChatUserComponent } from './online-chat-user.component';

describe('OnlineChatUserComponent', () => {
  let component: OnlineChatUserComponent;
  let fixture: ComponentFixture<OnlineChatUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineChatUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineChatUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
