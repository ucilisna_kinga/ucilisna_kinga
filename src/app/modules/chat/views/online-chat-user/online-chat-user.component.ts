import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-online-chat-user',
  templateUrl: './online-chat-user.component.html',
  styleUrls: ['./online-chat-user.component.css']
})
export class OnlineChatUserComponent implements OnInit {

  @Input() user;
  @Input() animation = false;
  @Input() requestsWindow = false;


  constructor() { }

  ngOnInit() {
  }

}
