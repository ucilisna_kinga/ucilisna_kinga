import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-chat-thread-instance',
  templateUrl: './chat-thread-instance.component.html',
  styleUrls: ['./chat-thread-instance.component.css']
})
export class ChatThreadInstanceComponent implements OnInit {

  @Input() participants;
  @Input() active = 0;
  @Input() activeUser ;
  @Input() numberOfUnseenMessages ;
  @Input() lastMessage ;

  constructor() { }

  ngOnInit() {
  }

  getFullName(user) {
    let fullName = user.firstname + ' ' + user.lastname;
    if ( fullName.length > 21 ) {
      fullName = fullName.slice(0, 21) + '...';
    }
    return fullName;
  }

}
