import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMessageLineComponent } from './chat-message-line.component';

describe('ChatMessageLineComponent', () => {
  let component: ChatMessageLineComponent;
  let fixture: ComponentFixture<ChatMessageLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMessageLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMessageLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
