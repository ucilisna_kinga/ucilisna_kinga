import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../../../store/data-services/chat-service';
import {AuthTmpService} from '../../../../core/services/auth/auth-tmp-service';
import {ThreadTmpService} from '../../../../store/data-services/thread-tmp-service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-chat-main-container',
  templateUrl: './chat-main-container.component.html',
  styleUrls: ['./chat-main-container.component.css']
})
export class ChatMainContainerComponent implements OnInit {

  activeUser;
  activeUserThreads;
  activeThread;
  messagesByThread;

  constructor( public _authTmpService: AuthTmpService,
               private _chatService: ChatService,
               private _threadTmpService: ThreadTmpService,
               private _router: Router
  ) {
    this.activeUser = this._authTmpService.getActiveUser();
    // this.activeUserThreads = this._threadTmpService.getThreadsByUser(this.activeUser);
    // this.activeThread = this._threadTmpService.getActiveThread();

  }

  ngOnInit() {
    this._chatService.socket.emit('reqGetAllThreadsByUsername', {username: this.activeUser.username});

    this._chatService.socket.on('resGetAllThreadsByUsername', (threads) => {
      this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});
      if (threads.length > 0) {
        this.activeUserThreads = threads;
        this.activeThread = threads[0];

        this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});
        if (this._chatService.inMessagesRoute) {
          this._chatService.socket.emit('reqMakeThreadAndHisMessagesSeenAndUpdate',
            {threadId: this.activeThread.thread_id, userId: this.activeUser.username});
        }
      }

    });

    this._chatService.socket.on('resGetAllThreadsByUsernameForUpdate', (threads) => {
      if (threads.length > 0) {
        this.activeUserThreads = threads;

        this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});
        if (this._chatService.inMessagesRoute) {
          this._chatService.socket.emit('reqMakeThreadAndHisMessagesSeenAndUpdate',
            {threadId: this.activeThread.thread_id, userId: this.activeUser.username});
        }
      }

    });


    this._chatService.socket.on('resGetAllMessagesByThread', (messages) => {
      this.messagesByThread = messages;
      this.activeThread.last_message_text = this.messagesByThread[this.messagesByThread.length - 1].text;
    });

    this._chatService.socket.on('resMakeThreadAndHisMessagesSeenAndUpdate', (threads) => {
      this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});
      if (threads.length > 0) {
        this.activeUserThreads = threads;

        this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});
      }
    });

    this._chatService.socket.on('resMakeThreadAndHisMessagesSeen', () => {
      this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});
    });

    this._chatService.socket.on('newMessageInserted', (data) => {
      // console.log('this._router.url');
      // console.log(this._router.url);
      // console.log('this._chatService.inMessagesRoute');
      // console.log(this._chatService.inMessagesRoute);

       if (data.threadId === this.activeThread.thread_id && this._chatService.inMessagesRoute) {
         this._chatService.socket.emit('reqMakeThreadAndHisMessagesSeen',
           {threadId: this.activeThread.thread_id, userId: this.activeUser.username});
         // this._chatService.socket.emit('reqGetAllThreadsByUsername', {username: this.activeUser.username});
         // this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});

         // if (this._chatService.inMessagesRoute) {
         //   this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});
         // }
       } else {
         this._chatService.socket.emit('reqGetAllThreadsByUsernameForUpdate', {username: this.activeUser.username});
         // this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});

         /*
         // this._chatService.socket.emit('reqGetAllMessagesByThread', {threadId: this.activeThread.thread_id});
         // update all threads by emit reqGetAllThreadsByUsername
         // update unreaded threads counter automatically done because  it listen on resGetAllThreadsByUsername
          */
       }
    });

    this._chatService.socket.on('resSendMessageSideBtn', (data) => {
      // console.log(data);
    });
  }




  onThreadChange(t) {
    this.activeThread = t;
    // debug
    // console.log('t : ' + t.thread_id);
    // console.log('this.activeThread: ' + this.activeThread.thread_id);
    // if (t.thread_id === this.activeThread.thread_id) console.log('true');
    // console.log('-----');


    this._threadTmpService.setActiveThread(t);

    this._chatService.socket.emit('reqMakeThreadAndHisMessagesSeenAndUpdate',
      {threadId: this.activeThread.thread_id, userId: this.activeUser.username});
    this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});

    // console.log(t.id);
    // refactor // put active thread in the thread service
    // active messages in messages service
    // change them here
  }



  onMessageSent(text) {
    let haveEmptyNewLines = true;
    const lines = text.split(/\r\n|\r|\n/g);

    for (const l of lines) {
      if (l.length > 0 ) {
        haveEmptyNewLines = false;
      }
    }
    if (text.length > 0 && !haveEmptyNewLines) {
      const data = {messageText: text, threadId: this.activeThread.thread_id, userId: this.activeUser.username};
      this._chatService.socket.emit('reqSaveMessageForThread', data);
    }
    // send msg to server
    // data =  msg,  active_thread
  }

}
