import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfOnlineChatUsersComponent } from './list-of-online-chat-users.component';

describe('ListOfOnlineChatUsersComponent', () => {
  let component: ListOfOnlineChatUsersComponent;
  let fixture: ComponentFixture<ListOfOnlineChatUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfOnlineChatUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfOnlineChatUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
