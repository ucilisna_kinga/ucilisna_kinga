import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-friends-requests',
  templateUrl: './friends-requests.component.html',
  styleUrls: ['./friends-requests.component.css']
})
export class FriendsRequestsComponent implements OnInit {

  // @Input() potencialFriends = [{ username: 'pf2', firstName:'firstName', avatarUrl: 'parent_male.jpg'},
  //   { username: 'pf2', firstName:'firstName', avatarUrl: 'parent_female.jpg'}];
  @Input() potencialFriends = [];
  @Output() acceptFriendship = new EventEmitter();
  @Output() rejectFriendship = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clickAcceptFriendship(pf) {
    this.acceptFriendship.emit(pf);
  }

  clickRejectFriendship(pf) {
    this.rejectFriendship.emit(pf);
  }

}
