import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  @Output() userLoginInput = new EventEmitter();

  constructor() {
  }

  onUserLoginInput(username, password) {
    this.userLoginInput.emit({username: username, password: password});
  }

}
