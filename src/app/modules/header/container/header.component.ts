import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthTmpService} from '../../../core/services/auth/auth-tmp-service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Router} from '@angular/router';
import {ChatService} from '../../../store/data-services/chat-service';
import {UserService} from '../../../store/data-services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() spinnerValue = new EventEmitter();
  public showLoginAlert = false;
  public activeUser;
  public loginAlert = {
    type: 'danger',
    message: 'Погрешно корисничко име или лозинка. Обидете се повторно.'
  };
  searchedUsers;
  searchUsersText;
  public numOfUnreadedThreads = 0;
  showFriendsRequestWindow = false;
  friendsRequests = [];
  friendsRequestsLoadingDone = false;

  constructor(private _authTmpService: AuthTmpService,
              private _router: Router,
              private _chatService: ChatService,
              private _userService: UserService) {
  }

  ngOnInit() {
    // this._userService.getUserTmp().subscribe( r => console.log(r));

    this.activeUser = this._authTmpService.getActiveUser();
    if (this.activeUser) {
      this.getFriendsRequests();
      setInterval(this.getFriendsRequests.bind(this), 3000);
    }
  }

  onMidIconsCompStartInit() {
    this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});
    this._chatService.socket.on('resGetMessageCounter', (counter) => {
      this.numOfUnreadedThreads = counter;
    });

    this._chatService.socket.on('newMessageInserted', (data) => {
      if (!this._chatService.inMessagesRoute) {
        this._chatService.socket.emit('reqGetMessageCounter', {username: this.activeUser.username});
      }
    });
    this._router.events.subscribe((val) => {

      for (const p in val) {
        if (p.toString() === 'url') {
          if (val[p].endsWith('messages')) {
            this._chatService.inMessagesRoute = true;
          } else {
            this._chatService.inMessagesRoute = false;
          }
        }
      }
    });

  }

  logInUser(username, password) {
    this.spinnerValue.emit('start');

    this._authTmpService.logInUser(username, password)
      .subscribe( res => {
        this.spinnerValue.emit('stop');
        if (res.message) {
          // console.log('res.message: ' + res.message);
          this._authTmpService.setUserLoggedIn(false);
          this.showLoginAlert = true;
        } else if (res.token) {
          // console.log('res.token: ' + res.token);
          this._authTmpService.setToken(res.token);

          this._userService.getUser()
            .subscribe( u => {
              this._authTmpService.setActiveUser(u);
              this.activeUser = this._authTmpService.getActiveUser();
              this._authTmpService.setUserLoggedIn(true);

              this._chatService.openSocket(username);
              // this._chatService.socket.emit('reqGetAllActiveUsers', username);
              // this._router.navigateByUrl('/profile/wall',username);
              this._router.navigate([username, 'wall']); // use it without slashes , router add / for each item in the array
            });
        }

      });
  }

  logOutUser() {
    this._authTmpService.logOutUser();
    this._chatService.closeSocket();
  }
  isUserLoggedIn() {
    return this._authTmpService.isUserLoggedIn();
  }

  inputSearchUsers(text) {
    if (text.length > 0) {
      this._userService.searchUsers(text)
        .subscribe( res => {
          this.searchedUsers = res;
        });
    }
  }

  onAcceptFriendship(friend) {
    this._chatService.socket.emit('reqInsertFriendship', {user: this.activeUser, friend: friend});

    this._userService.acceptFriendship(friend.username)
      .toPromise().then(() => {
        this.getFriendsRequests();
    });

  }

  onRejectFriendship(friend) {
    this._userService.deleteUserFriend(friend.username)
      .toPromise().then(() => {
        this.getFriendsRequests();
    });
  }


  getFriendsRequests() {
    this._userService.getFriendRequests(this.activeUser.username).subscribe( users => {
      this.friendsRequestsLoadingDone = true;
      this.friendsRequests = users;
    });
  }

}
