import {Component, OnInit} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import { ActivatedRoute, Params } from '@angular/router';
import {UserService} from '../../store/data-services/user.service';

@Component({
  selector: 'app-profile-details-page',
  templateUrl: './profile-details-page.component.html',
  styleUrls: ['./profile-details-page.component.css']
})
export class ProfileDetailsPageComponent implements OnInit {

  public activeUser;
  public wallUser;
  doneLoadingUser = false;

  constructor(private _authTmpService: AuthTmpService,
              private userService: UserService,
              private _route: ActivatedRoute) {
    this.activeUser = this._authTmpService.getActiveUser();
  }

  isUserLoggedIn() {
    return this._authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this._route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }
        });
    });
  }

}
