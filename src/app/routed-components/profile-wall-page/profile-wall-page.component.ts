import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import {ActivatedRoute, Params } from '@angular/router';
import {UserService} from '../../store/data-services/user.service';
import {PostService} from '../../store/data-services/post.service';
import {UiStateService} from '../../core/services/ui-state.service';

@Component({
  selector: 'app-profile-wall-page',
  templateUrl: './profile-wall-page.component.html',
  styleUrls: ['./profile-wall-page.component.css']
})
export class ProfileWallPageComponent implements OnInit {

  public activeUser;
  public wallUser;
  public wallPosts;
  displayPostsFirstTime;
  doneLoadingUser;
  doneLoadingPosts;

  constructor(private authTmpService: AuthTmpService,
              private userService: UserService,
              private postService: PostService,
              private route: ActivatedRoute) {
    this.activeUser = this.authTmpService.getActiveUser();
  }

  isUserLoggedIn() {
    return this.authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this.displayPostsFirstTime = true;
    this.doneLoadingUser = false;
    this.doneLoadingPosts = false;
    this.route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }

          this.getAllWallUserPosts(this.wallUser.username);
        });
    });
  }

  getAllWallUserPosts( username ) {
    this.doneLoadingPosts = false;

    this.postService.getAllPostByUsername(username)
      .then( ps => {
        this.postService.sortPostsByDate(ps);
        ps.forEach( p => {
          p.creatorOfPost = this.wallUser;
          p.timeCreated = this.postService.convertTime(p.timeCreated);
        });

        this.wallPosts = ps;
        this.doneLoadingPosts = true;
      });
  }

  onCreatedPost(text) {
    this.displayPostsFirstTime = false;
    if (text.length > 0) {
      this.postService.savePost(this.activeUser, text)
        .subscribe( () => {
          this.getAllWallUserPosts(this.activeUser.username);
        });
    }

  }

  canCreatePost() {
    if ( this.wallUser.username === this.activeUser.username ) { return true; } else {
      return false;
    }
  }



}
