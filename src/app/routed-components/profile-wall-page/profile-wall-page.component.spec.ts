import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileWallPageComponent } from './profile-wall-page.component';

describe('ProfileWallPageComponent', () => {
  let component: ProfileWallPageComponent;
  let fixture: ComponentFixture<ProfileWallPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileWallPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileWallPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
