import {Component, Input, OnInit} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import { ActivatedRoute, Params } from '@angular/router';
import {UserService} from '../../store/data-services/user.service';

@Component({
  selector: 'app-profile-friends-page',
  templateUrl: './profile-friends-page.component.html',
  styleUrls: ['./profile-friends-page.component.css']
})
export class ProfileFriendsPageComponent implements OnInit {

  activeUser;
  userFriends;
  wallUser;
  doneLoadingUser = false;
  doneLoadingUserFriends = false;

  constructor(private _authTmpService: AuthTmpService,
              private userService: UserService,
              private _route: ActivatedRoute) {
    this.activeUser = this._authTmpService.getActiveUser();

  }

  isUserLoggedIn() {
    return this._authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this._route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }
        });

      this.userService.getUserFriendsByUsername(params['username'])
        .subscribe( uf => {
          this.doneLoadingUserFriends = true;
          this.userFriends = uf;
        });
      // this.wallUser = this._userTmpService.getUserByUsername(params['username']);
      // this.userFriends = this._userTmpService.getUserFriends(this.wallUser);
    });
  }

}
