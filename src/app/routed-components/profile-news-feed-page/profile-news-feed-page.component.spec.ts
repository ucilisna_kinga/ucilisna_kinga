import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileNewsFeedPageComponent } from './profile-news-feed-page.component';

describe('ProfileNewsFeedPageComponent', () => {
  let component: ProfileNewsFeedPageComponent;
  let fixture: ComponentFixture<ProfileNewsFeedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileNewsFeedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileNewsFeedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
