import {Component, OnInit} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import { ActivatedRoute, Params } from '@angular/router';
import {UserService} from 'app/store/data-services/user.service';

@Component({
  selector: 'app-profile-messages-page',
  templateUrl: './profile-messages-page.component.html',
  styleUrls: ['./profile-messages-page.component.css']
})
export class ProfileMessagesPageComponent implements OnInit {

  public activeUser;
  public wallUser;
  doneLoadingUser = false;

  constructor(private _authTmpService: AuthTmpService,
              private userService: UserService,
              private _route: ActivatedRoute) {
    this.activeUser = this._authTmpService.getActiveUser();
  }

  isUserLoggedIn() {
    return this._authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this._route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }
        });
    });
  }

}
