import { Component, ChangeDetectionStrategy } from '@angular/core';
import {BookTmpService} from '../../../store/data-services/book-tmp-service';

@Component({
  selector: 'app-my-book-collections-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-book-collections-page.component.html',
  styleUrls: ['./my-book-collections-page.component.css']
})
export class MyCollectionsPageComponent {
  books;

  constructor(private bookTmpService: BookTmpService) {
      this.books = this.bookTmpService.getBooks();
  }

}
