
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {environment} from './../../../../environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthTmpService {

//  private host = 'http://localhost:8080/';
  // private host =  'http://localhost:10000/api/user/';
  // private host = 'http://178.238.235.229:8081/';
  private host = environment.authServiceIpAddress;


  private userLoggedIn = false;
  private activeUser;
  private token;
  constructor(private http: Http) { }

  logInUser(username, password) {
    return this.http.post(this.host + 'auth', {
      username: username,
      password: password
    }).map(res => res.json()) ;
  }
  logOutUser() {
    this.userLoggedIn = false;
  }

  isUsernameExist(username) {
    return this.http.get(this.host + `/auth/checkUsername/${username}`).map(res => res.json());
  }

  registerUser(user) {
    return this.http.put(this.host + 'auth/registerUser', user).map(res => res.json());
  }

  isUserLoggedIn() {
    return this.userLoggedIn;
  }

  setUserLoggedIn(value: boolean) {
    this.userLoggedIn = value;
  }

  getActiveUser() {
    return this.activeUser;
  }

  setActiveUser(user) {
    this.activeUser = user;
  }
  getToken() {
    return this.token;
    // return localStorage.getItem('token');
  }
  setToken(t) {
    this.token = t;
    // localStorage.setItem('token', t);
  }

}
