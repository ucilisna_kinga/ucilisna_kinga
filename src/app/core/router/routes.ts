import {Routes} from '@angular/router';
import {SingInPageComponent} from '../../routed-components/sing-in-page/sing-in-page.component';
import {MyCollectionsPageComponent} from '../../routed-components/_book/my-collections-page/my-book-collections-page.component';
import {BookHomePageComponent} from '../../routed-components/_book/book-home-page/book-home-page.component';
import {ProfileWallPageComponent} from '../../routed-components/profile-wall-page/profile-wall-page.component';
import {ProfileFriendsPageComponent} from '../../routed-components/profile-friends-page/profile-friends-page.component';
import {ProfileNewsFeedPageComponent} from '../../routed-components/profile-news-feed-page/profile-news-feed-page.component';
import {ProfileMessagesPageComponent} from '../../routed-components/profile-messages-page/profile-messages-page.component';
import {ProfileDetailsPageComponent} from '../../routed-components/profile-details-page/profile-details-page.component';

export const routes: Routes = [
  { path: '', component: SingInPageComponent},
  { path: 'sing-in', component: SingInPageComponent},
  { path: ':username/news-feed', component: ProfileNewsFeedPageComponent},
  { path: ':username/messages', component: ProfileMessagesPageComponent},
  { path: ':username/wall', component: ProfileWallPageComponent},
  { path: ':username/details', component: ProfileDetailsPageComponent},
  { path: ':username/friends', component: ProfileFriendsPageComponent},
  { path: 'search-book', component: BookHomePageComponent},
  { path: 'my-book-collections', component: MyCollectionsPageComponent},
];
