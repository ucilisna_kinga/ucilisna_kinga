import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from './../../../environments/environment';

@Injectable()
export class ChatService {

  public inMessagesRoute = false;
  public socket = null;
  public onlineUserFriendsForActiveUser = [];

  public openSocket(username) {
    this.socket = io(environment.chatHostIpAddress,  { query: `username=${username}` });
    // console.log('envir_chat_ip');
    // console.log(environment.chatHostIpAddress);

  }
  public closeSocket() {
    this.socket.disconnect();
  }
}
