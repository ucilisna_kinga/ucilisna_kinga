
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Book} from '../models/db-models/book';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {of} from 'rxjs/observable/of';


@Injectable()
export class BookTmpService {

  private API_PATH = 'https://www.googleapis.com/books/v1/volumes';
  // private API_PATH = 'https://www.googleapis.com/books/v1/volumes?q=flowers';

  books = [
    {
      id: 'idsample1',
      volumeInfo: {
        title: 'Reactive Programming with Angular and ngrx 2',
        description: `<p>Manage your Angular development using Reactive programming. 
Growing in popularity and now an essential part of any professional web developer's toolkit, 
Reactive programming can enrich your development and make your code more efficient.
</p><p>Featuring a core application to explore and build yourself, this book shows you how to utilize
 ngrx/store as a state management with Redux pattern, and how to utilize ngrx/effects to define a better 
 and more robust application architecture. Through working code examples, you will understand every aspect
  of Reactive programming with Angular so that you'll be able to develop maintainable, readable code.</p><p>
   </p><p><i>Reactive Programming with Angular and ngrx</i>  is ideal for developers already familiar 
   with JavaScript, Angular, or other languages, and who are looking for more insight into their Angular projects. 
   Use this book to start mastering Reactive programming today.</p><b>What You'll Learn</b>see how="" the="" 
   boilerplate="" and="" webpack="" work p/pulliConstruct components efficientlybr/liliUtilize ngrx extensions 
   and RxJSbr/liliOrganize state management with reducers, actions and side effectsbr/li/ulp</p></p> <p></p><b>Who
    This Book Is For</b><br>Developers who are already familiar with JavaScript
 and Angular and want to move onto more advanced development. <br><br>`,
      }
    },
    {
      id: 'idsample3',
      volumeInfo: {
        title: 'title 3',
        description: 'description3'
      }
    },
  ];

  constructor(private http: Http ) {}

  public getBooks() {
    return this.books;
  }

  public searchBooks(queryString): Observable<Book[]> {
    return this.http.get(`${this.API_PATH}?q=${queryString}`)
      .map(res => res.json().items || []);
  }
}
