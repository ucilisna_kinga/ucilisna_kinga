export interface Book {

  id: string;
  volumeInfo: {
    title: string,
    description: string
  };

}
