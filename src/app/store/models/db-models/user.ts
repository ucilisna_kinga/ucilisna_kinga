
export interface User {
  id: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  gender: string;
  friendsIds: string[];
  profileType: string;
}
