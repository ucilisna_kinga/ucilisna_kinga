import {EffectsModule} from '@ngrx/effects';
import {BookEffects} from './book-effects';

export const AppEffectsRun = [
  EffectsModule.run(BookEffects)
];

// ... todo in AppModule
// imports: [
//   ...appEffectsRun
// ]
