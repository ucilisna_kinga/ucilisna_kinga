import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {Effect, Actions, toPayload} from '@ngrx/effects';
import {BookTmpService} from 'app/store/data-services/book-tmp-service';
import * as book from '../actions/book-actions';
import {empty} from 'rxjs/observable/empty';
import {of} from 'rxjs/observable/of';

@Injectable()
export class BookEffects {

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(book.SEARCH)
    .debounceTime(300)
    .map(toPayload)
    .switchMap( query => {
      if (query === '') {
        return empty();
      }

      // const nextSearch$ = this.actions$.ofType(book.SEARCH).skip(1);

      return this.bookTmpService.searchBooks(query)
        // .takeUntil(nextSearch$)
        .map(books => new book.SearchCompleteAction(books))
        .catch(() => of(new book.SearchCompleteAction([])));
    } );

  constructor(private actions$: Actions, private bookTmpService: BookTmpService) { }
}
