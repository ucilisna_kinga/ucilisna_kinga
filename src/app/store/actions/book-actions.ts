

// constants
import {Action} from '@ngrx/store';
import {Book} from '../models/db-models/book';

export const SEARCH =           '[BOOK] Search';
export const SEARCH_COMPLETE =  '[BOOK] Search Complete';

// actions classes
export class SearchAction implements Action {
  readonly type = SEARCH;
  constructor(public payload: string) { }
}

export class SearchCompleteAction implements Action {
  readonly type = SEARCH_COMPLETE;
  constructor(public payload: Book[]) { }
}


// export type

export type Actions
  = SearchAction
  | SearchCompleteAction ;
