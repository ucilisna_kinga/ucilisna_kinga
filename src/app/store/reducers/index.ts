import {ActionReducer, combineReducers} from '@ngrx/store';
import {compose} from '@ngrx/core';
import {storeFreeze} from 'ngrx-store-freeze';


// import reducers class with alias
import * as fromBooks from './books';
import {environment} from '../../../environments/environment';

// all states
export interface State {
  books: fromBooks.State;
}

// all reducers
const reducers = {
  books: fromBooks.reducer
};

// dev mode reducer
const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);

// prod mode reducer
const productionReducer: ActionReducer<State> = combineReducers(reducers);

// export reducer
export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}



// selectors

// global selectors
export const getBooksState = (state: State) => state.books;

// specific selectors
